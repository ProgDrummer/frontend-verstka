$(document).ready(function(){
    $(".owl-two").owlCarousel({
        autoplay:false,
        loop:true,
        margin:60,
        dots:true, 
        nav:true, 
        navText:['<img src="img/prev1.png" />','<img src="img/next1.png" />'], 
        responsive:{
            0:{items:1},
            600:{items:1},
            1000:{items:1}
        }

    })

})

$(document).ready(function(){
    $(".owl-one").owlCarousel({
        autoplay:false,
        loop:true,
        margin:60,
        dots:false, 
        nav:true, 
        navText:['<img src="img/prev2.png" />','<img src="img/next2.png" />'], 
        responsive:{
            0:{items:1},
            600:{items:1},
            1000:{items:1}
        }


    })

})  
$(document).ready(function(){
    $(".owl-three").owlCarousel({
        autoplay:false,
        loop:true,
        margin:26,  
        dots:false,
        nav:true, 
        navText:['<img src="img/prev3.png" />','<img src="img/next3.png" />'], 
        responsive:{
            0:{items:1},
            600:{items:2},
            1000:{items:3}
        }


    })

}) 

$(document).ready(function(){
    $(".owl-four").owlCarousel({
        autoplay:false,
        loop:true,
        margin:60,
        dots:false, 
        nav:true, 
        navText:['<img src="img/prev4.png" />','<img src="img/next4.png" />'], 
        responsive:{
            0:{items:1},
            600:{items:1},
            1000:{items:1}
        }


    })

})

$(document).ready(function(){
    $(".owl-five").owlCarousel({
        autoplay:false,
        loop:true,
        margin:15,
        dots:false, 
        nav:true, 
        navText:[' ',' '], 
        responsive:{
            0:{items:1},
            600:{items:2},
            1000:{items:3}
        }


    })

})

$(document).ready(function(){
    $(".owl-six").owlCarousel({
        autoplay:false,
        loop:true,
        margin:0,
        dots:false, 
        nav:true, 
        navText:['','NEXT'], 
        responsive:{
            0:{items:1},
            600:{items:1},
            1000:{items:1}
        }


    })

})