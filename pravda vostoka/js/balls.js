$(document).ready(function(){
    $(".owl-two").owlCarousel({
        autoplay:true,
        loop:true,
        margin:60,
        dots:false,
        responsive:{
            0:{items:1},
            600:{items:1},
            1000:{items:1}
        }

    })

})

$(document).ready(function(){
    $(".owl-one").owlCarousel({
        autoplay:false,
        loop:true,
        margin:60,
        dots:false, 
        nav:true, 
        navText:['&lt;','&gt;'], 
        responsive:{
            0:{items:1},
            400:{items:2},
            500:{items:3},
            1000:{items:3}
        }

    })

})  